package br.pucpr.bsi.projetoIntegrador.nomeProjeto.dao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.pucpr.bsi.projetoIntegrador.nomeProjeto.dao.util.HibernateUtil;
import br.pucpr.bsi.projetoIntegrador.nomeProjeto.model.Artista;

public class ArtistaDAO extends PatternDAO<Artista> {

	private static final long serialVersionUID = 1L;
	private final static ArtistaDAO instance = new ArtistaDAO();

	private ArtistaDAO() {
		super(Artista.class);
	}

	public static ArtistaDAO getInstance() {
		return instance;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Artista> findByFilter(Artista filter) {
		Session session = HibernateUtil.getSession();
		try {
			Criteria c = session.createCriteria(entityClassName);
			if (StringUtils.isNotBlank(filter.getNome())) {
				c.add(Restrictions.like("nome", "%" + filter.getNome() + "%"));
			}
			return c.list();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}
