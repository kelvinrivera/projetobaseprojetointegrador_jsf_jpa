package br.pucpr.bsi.projetoIntegrador.nomeProjeto.tests.util;

import br.pucpr.bsi.projetoIntegrador.nomeProjeto.model.Artista;

/**
 * Classe responsavel por atualizar informacoes para update e comparacoes
 * @author Mauda
 *
 */
public class Atualizador {
	
	/**
	 * Metodo responsavel por atualizar informacoes do objeto
	 * @param objeto
	 * @param cod
	 */
	public static void atualizar(Artista objeto, String cod){
		objeto.setNome(cod 		+ objeto.getNome());
	}
	
}
